# Tidsbanken Frontend

Angular frontend for _Experis Academy Case: Tidsbanken_ - Winter 2020.

By **Felix Grimsrud**, **Nikolai Norum Hansen** & **Sondre Kindem**

## Live version
**[https://sondrekindem.gitlab.io/academy-tidsbanken-frontend](https://sondrekindem.gitlab.io/academy-tidsbanken-frontend)**

## Manual
[Check out the manual in the wiki](https://gitlab.com/SondreKindem/academy-tidsbanken-frontend/-/wikis/User-manual)

## Set up

Clone repo and run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build --prod --base-href /academy-tidsbanken-frontend/` to build the project in production mode. The base-href must be the name of the folder where the app is served. The build artifacts will be stored in the `dist/` directory. Check out the `.gitlab-ci.yml` file for how we deploy.
