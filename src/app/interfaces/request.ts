
export interface Request {
  id: number;
  title: string;
  periodStart: Date;
  periodEnd: Date;
  ownerId: string;
  ownerName: string;
  state: string;
  moderatorId?: string;
  moderatorName?: string;
  moderatedAt: Date;
}
