export interface User {
  id: string;
  name?: string;
  email: string;
  isAdmin: boolean;
  image: string;
  firstName: string;
  lastName: string;
}
