export interface Comment {
  id: number;
  message: string;
  createdOn: Date;
  updatedOn?: Date;
  requestId: number;
  userId: string;
  userName: string;
}
