import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BootstrapIconsModule} from 'ng-bootstrap-icons';
import {XSquare, PencilSquare, Check, X, Trash, Pencil, Plus} from 'ng-bootstrap-icons/icons';

const icons = {
  XSquare,
  Pencil,
  PencilSquare,
  Check,
  X,
  Trash,
  Plus
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BootstrapIconsModule.pick(icons)
  ],
  exports: [
    BootstrapIconsModule
  ]
})
export class IconsModule {
}
