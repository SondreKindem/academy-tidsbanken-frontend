import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RequestComponent } from './components/request/request.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {LoginGuard} from './services/login.guard';
import {SettingsComponent} from './components/settings/settings.component';
import {AdminGuard} from './services/admin.guard';
import {ManagementComponent} from './components/management/management.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [LoginGuard],
    children: [
      {
        path: "",
        component: DashboardComponent,
      },
      {
        path: "profile",
        component: UserProfileComponent,
      },
      {
        path: "profile/:id",
        component: UserProfileComponent,
      },
      {
        path: "settings",
        component: SettingsComponent,
        canActivate: [AdminGuard]
      },
      {
        path: `request/:id`,
        component: RequestComponent
      },
      {
        path: `management`,
        component: ManagementComponent,
        canActivate: [AdminGuard]
      },
    ]
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
