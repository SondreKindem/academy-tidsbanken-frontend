import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {User} from '../../interfaces/user';
import {Router} from '@angular/router';
import {Request} from '../../interfaces/request';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isNavbarCollapsed = true;

  constructor(private userService: UserService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  get userIsAdmin(): boolean{
    return this.userService.checkUser();
  }

  get user(): User{
    return this.userService.user ?? {name: "", isAdmin: false, image: "", email: "", id: "", firstName: "", lastName: ""};
  }

  logOut(): void{
    this.userService.logOut();
    this.router.navigate(['login']);
  }

  openCreateModal(modal): void {
    this.modalService.open(modal);
  }

  handleRequestCreated(modal, request: Request): void {
    this.modalService.dismissAll();
    this.router.navigate(["/request", request.id]);
  }
}
