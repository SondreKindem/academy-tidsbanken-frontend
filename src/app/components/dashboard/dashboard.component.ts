import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FullCalendarComponent, CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import {
  BaseComponent,
  Calendar,
  DateSelectArg,
  EventApi,
  EventClickArg,
  EventInput
} from '@fullcalendar/core';
import { DayGridView } from '@fullcalendar/daygrid';
import { RequestService } from '../../services/request/request.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IneligibleService } from '../../services/ineligible/ineligible.service';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('createIneligibleModal') createIneligibleModal: ElementRef;
  @ViewChild('updateIneligibleModal') updateIneligibleModal: ElementRef;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  loading = true;
  savedRange: { from: Date; to: Date };
  modalsNumber = 0;
  calendarOptions: CalendarOptions = {
    timeZone: 'UTC',
    height: 'auto',
    initialView: 'dayGridMonth',
    firstDay: 1,
    selectable: true,
    editable: true,
    selectMirror: true,
    dayMaxEvents: true,
    eventBackgroundColor: '#ef4f2f',
    eventClick: this.handleEventClick.bind(this),
    customButtons: {
      myCustomButton: {
        text: 'create ineligible period',
        click: () => this.open(this.createIneligibleModal)
      }
    },
    headerToolbar: {
      left: 'prev next today',
      center: 'title',
      right: 'myCustomButton'
    }
  };

  calendarEvents: EventInput[] = [{ start: new Date(), end: new Date() }];
  currentEvents: EventApi[] = [];

  constructor(
    private requestService: RequestService,
    private modalService: NgbModal,
    private ineligible: IneligibleService,
    private userService: UserService,
    private router: Router
  ) {
    this.modalService.activeInstances.subscribe((list) => {
      this.modalsNumber = list.length;
    });
  }

  //modal for creating a new ineligible period
  open(createIneligibleModal) {
    this.modalService
      .open(createIneligibleModal, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {
        // this.savedRange.to.setHours(this.savedRange.to.getHours() + 23);
        // this.savedRange.to.setMinutes(this.savedRange.to.getMinutes() + 59);
        this.savedRange.to.setDate(this.savedRange.to.getDate() + 1);

        //Sends ineligible period to api
        return this.ineligible.createIneligible(this.savedRange);
      })
      .then((result: any) => {
        console.log(result);
        //adds the period to the local calendar, and re-renders it
        this.calendarComponent.getApi().addEvent({
          id: result.data.id,
          start: this.savedRange.from,
          end: this.savedRange.to,
          allDay: true,
          display: 'background',
          eventBackGround: 'red'
        });
        this.currentEvents = this.calendarComponent.getApi().getEvents();
        this.calendarComponent.getApi().render();
      })
      .catch((result) => {
        console.error(result);
      });
  }

  //is called by the modal
  saveRange(range) {
    this.savedRange = range;
  }

  handleEventClick(clickInfo: EventClickArg) {
    //take user to user page
    if (clickInfo.event.display === 'auto') {
      let requestId = clickInfo.event.id;
      requestId = requestId.replace('request', '');
      this.router.navigate([`/request`, requestId]);
    }
    //let admin edit ineligible periods (events with display=background)
    if (
      this.userService.checkUser() &&
      clickInfo.event.display === 'background'
    ) {
      //put all below in separate onIneligibleEvent(period)Click-function
      this.modalService
        .open(this.updateIneligibleModal, {
          ariaLabelledBy: 'modal-basic-title'
        })
        .result.then((result) => {
          var id = parseInt(clickInfo.event.id);
          if (result === 'delete') {
            this.ineligible
              .deleteIneligible(id)
              .then(() => {
                clickInfo.event.remove();
              })
              .catch((e) => {
                console.error(e);
              });
          } else {
            this.savedRange.to.setDate(this.savedRange.to.getDate() + 1);
            //sends patch-request to the api and re-renders the calendar with updated events(periods)
            //there was no update function for background events in fullcalendar, so we have to delete and re-create them
            this.ineligible
              .updateIneligible(id, this.savedRange)
              .then((result: any) => {
                console.log(id);
                console.log(this.savedRange.to);
                console.log(result.data.end);
                clickInfo.event.remove();
                this.calendarComponent.getApi().addEvent({
                  id: result.data.id,
                  start: this.savedRange.from,
                  end: this.savedRange.to,
                  allDay: true,
                  display: 'background',
                  eventBackGround: 'red'
                });
                console.log(id);
                this.currentEvents = this.calendarComponent
                  .getApi()
                  .getEvents();
                this.calendarComponent.getApi().render();
              })
              .catch((e) => {
                console.error(e);
              });
          } //end else
        })
        .catch((e) => {
          console.error(e);
        });
    } //end if
  }

  ngOnInit(): void {
    //removes ineligible button if the user isn't admin
    if (this.userService.checkUser() === false) {
      this.calendarOptions.customButtons = {};
      this.calendarOptions.headerToolbar = {
        left: 'prev next today',
        center: 'title',
        right: ''
      };
    }
  }

  //actually renders the calendar, not in ngInit since it wasn't being instantiated quick enough
  ngAfterViewInit(): void {
    var calendar = this.calendarComponent.getApi();
    this.ineligible
      .fetchAll()
      .then((result: any) => {
        result.data.forEach((element) => {
          const test = new Date(element.end);
          //test.setDate(test.getDate() + 1);

          calendar.addEvent({
            id: element.id,
            start: element.start,
            end: test,
            allDay: true,
            display: 'background'
          });
        });
        this.currentEvents = calendar.getEvents();

        this.requestService.allRequests.forEach((element) => {
          let color = '';
          switch (element.state) {
            case 'Pending':
              color = '#dbb300';
              break;
            case 'Approved':
              color = '#15cb00';
              break;
            case 'Denied':
              color = '#e34444';
              break;
            default:
              color = 'grey';
          }
          const end = new Date(element.periodEnd);
          end.setDate(end.getDate() + 1);

          calendar.addEvent({
            id: `request${element.id}`,
            color: color,
            title: `${element.title}`,
            start: element.periodStart,
            end,
            allDay: true,
            display: 'auto'
          });
        });

        calendar.render();
        this.loading = false;
      })
      .catch((e) => {
        console.error(e);
      });
  }
}
