import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../../services/request/request.service';
import {Request} from '../../interfaces/request';
import {UserService} from '../../services/user/user.service';
import {Comment} from '../../interfaces/comment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {

  isLoading: boolean;

  request: Request = null;
  comments: Comment[];

  newComment = '';

  constructor(private route: ActivatedRoute, private requestService: RequestService, private userService: UserService, private modalService: NgbModal, private router: Router) {
  }

  async ngOnInit(): Promise<any> {
    this.isLoading = true;
    // Get id from params, then fetch stuff
    this.route.params.subscribe(async params => {
      // Will be null if no access
      this.request = await this.requestService.getRequest(+params['id']);

      // Only owner and admin can see comments. Don't fetch if there is no request
      if (this.request && this.userCanSeeComments()) {
        this.comments = await this.requestService.getComments(this.request.id);
      }

      this.isLoading = false;
    });
  }

  addComment(): void {
    this.requestService.addComment(this.request.id, this.newComment).then(
      (comment: Comment) => {
        if (comment) {
          this.comments.push(comment);
        }
      }
    );
  }

  openUpdateModal(modal): void {
    this.modalService.open(modal);
  }

  handleRequestUpdated(request: Request): void {
    if (request) {
      this.request = request;
    }
    this.modalService.dismissAll();
  }

  deleteRequest(modal): void {
    this.modalService.open(modal).result.then(
      () => {
        this.requestService.deleteRequest(this.request.id).then(
          (success) => {
            if (success) {
              this.router.navigate(['/']);
            }
          }
        );
      },
      () => {
      }
    );
  }

  userCanAccept(): boolean {
    // Only admins can accept or deny, and admin cannot accept their own request
    return this.userService.checkUser() && this.request.ownerId !== this.userService.user.id;
  }

  userCanEdit(): boolean {
    // Only admin and owner can edit request
    if (this.request.state === 'Approved') {
      // If the request is approved, only admins can edit
      return this.userService.checkUser() && this.request.ownerId !== this.userService.user.id;
    }

    return this.userService.checkUser() || this.request.ownerId === this.userService.user.id;
  }

  userCanSeeComments(): boolean {
    // Only admin and owner can see comments
    return this.userService.checkUser() || this.request.ownerId === this.userService.user.id;
  }

  userCanViewRequest(): boolean {
    // Only admin and
    // owner can view while the request is not approved
    if (this.isLoading) {
      // Always show while loading
      return true;
    }
    return this.request && (this.userCanEdit() || this.request.state === 'Approved');
  }

  canEditComment(comment: Comment): boolean {
    // Only the owner of the comment can edit
    if (comment.createdOn) {
      return comment.userId === this.userService.user.id;
    }
    return false;
  }

  approveRequest(): void {
    this.requestService.setRequestState(this.request.id, 'Approved').then(
      (result) => {
        if (result) {
          this.request.state = 'Approved';
        }
      }
    );
  }

  denyRequest(): void {
    this.requestService.setRequestState(this.request.id, 'Denied').then(
      (result) => {
        if (result) {
          this.request.state = 'Denied';
        }
      }
    );
  }

}
