import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../../interfaces/comment';
import {RequestService} from '../../services/request/request.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment;
  @Input() canEdit: boolean;

  editing = false;
  loading = false;
  editedComment: string;

  constructor(private requestSertice: RequestService) {
  }

  ngOnInit(): void {
    this.editedComment = this.comment.message;
  }

  updateComment(): void {
    if (this.editedComment === this.comment.message) {
      return;
    }

    this.loading = true;
    this.requestSertice.updateComment(this.comment.requestId, this.comment.id, this.editedComment).then(
      (result) => {
        this.editing = false;
        this.loading = false;
        if (result) {
          this.comment = result;
        }
      }
    );
  }

}
