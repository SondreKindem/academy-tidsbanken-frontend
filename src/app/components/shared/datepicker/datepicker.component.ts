import {Component, Output, EventEmitter, Input, OnInit} from '@angular/core';
import {NgbCalendar, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {SettingsService} from '../../../services/settings/settings.service';
import {IneligibleService} from '../../../services/ineligible/ineligible.service';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
/**
 * A datepicker with range select.
 * @emits 'rangeSelected' as an object: {from: Date, to: Date}
 */
export class DatepickerComponent implements OnInit {

  @Input() initialStart: Date;
  @Input() initialEnd: Date;

  @Output() rangeSelected = new EventEmitter<{ from: Date, to: Date }>();
  @Input() isIneligible: boolean;


  fromDate: NgbDate;
  toDate: NgbDate | null = null;
  hoveredDate: NgbDate | null = null;

  ineligible: { id, start: NgbDate, end: NgbDate }[] = [];

  markDisabled;

  constructor(private calendar: NgbCalendar, private settingsService: SettingsService, private ineligibleService: IneligibleService) {
  }

  async ngOnInit(): Promise<any> {
    this.fromDate = this.initialStart ? this.convertToNgbDate(this.initialStart) : null;
    this.toDate = this.initialEnd ? this.convertToNgbDate(this.initialEnd) : null;
    this.resetDisabled();
    this.ineligible = await this.ineligibleService.fetchAll().then(
      (result: { data: { id, start, end }[] }) => {
        result.data.map(
          (period) => {
            period.start = this.convertToNgbDate(new Date(period.start));
            period.end = this.convertToNgbDate(new Date(period.end));
            return period;
          }
        );
        return result.data;
      }
    );
    this.modifyDisabled();
    console.log(this.ineligible);
  }

  /**
   * Runs every time the user clicks a date.
   * Range selection is a bit of a hack.
   * @param date the selected date
   */
  onDateSelection(date: NgbDate): void {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      if (this.isIneligible !== true) {
        this.modifyDisabled();
      }
    } else if (this.fromDate && !this.toDate && (date.equals(this.fromDate) || date.after(this.fromDate))) {
      this.toDate = date;
      // Allow selecting new dates
      this.resetDisabled();
      // Emit event for range selection
      this.rangeSelected.emit({from: this.convertToDate(this.fromDate), to: this.convertToDate(this.toDate)});
    } else {
      this.toDate = null;
      this.fromDate = date;
      if (this.isIneligible !== true) {
        this.modifyDisabled();
      }
    }
  }

  convertToDate(ngbDate: NgbDate): Date {
    return new Date(Date.UTC(ngbDate.year, ngbDate.month - 1, ngbDate.day));
  }

  convertToNgbDate(date: Date): NgbDate {
    return new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
  }

  resetSelection(): void {
    // Reset markDisabled to prevent crashing
    this.resetDisabled();

    this.toDate = null;
    this.fromDate = null;

    // Emit null, so any parent knows that there is no longer a selection
    this.rangeSelected.emit({from: null, to: null});
  }

  /**
   * Set markDisabled to only disable weekends
   */
  resetDisabled(): void {
    this.markDisabled = (date: NgbDate) => this.calendar.getWeekday(date) >= 6 || this.dateIsIneligible(date);
  }

  dateIsIneligible(date: NgbDate): boolean {
    for (const period of this.ineligible) {
      if (date.before(period.end) && date.after(this.calendar.getPrev(period.start))) {
        return true;
      }
    }
  }

  /**
   * Update markDisabled to disable all dates past the max vacation length
   */
  modifyDisabled(): void {
    let maxDays = this.settingsService.getMaxVacationLength();

    const weekday = this.calendar.getWeekday(this.fromDate);

    // Algorithm from  https://stackoverflow.com/a/40739060
    // Exclude weekends in maxDays
    if (weekday + maxDays >= 6) {
      const remainingWorkDays = maxDays - (5 - weekday);
      maxDays += 2;
      if (remainingWorkDays > 5) {
        maxDays += 2 * Math.floor(remainingWorkDays / 5);
        if (remainingWorkDays % 5 === 0) {
          maxDays -= 2;
        }
      }
    }

    this.markDisabled = (date: NgbDate) => {
      return this.calendar.getWeekday(date) >= 6 || date.after(this.calendar.getNext(this.fromDate, 'd', maxDays - 1)) || this.dateIsIneligible(date);
    };
  }

  isHovered(date: NgbDate): boolean {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate): boolean {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate): boolean {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }
}
