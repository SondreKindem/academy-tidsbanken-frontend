import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from '../../../services/user/user.service';
import { RequestService } from '../../../services/request/request.service';
import { Router } from '@angular/router';
import { Request } from '../../../interfaces/request';

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.scss']
})
export class CreateRequestComponent implements OnInit {
  title: string;
  periodStart: Date;
  periodEnd: Date;

  comment = '';

  loading = false;

  @Output() create = new EventEmitter<Request>();

  constructor(
    private userService: UserService,
    private requestService: RequestService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.title = this.userService.user.name + ' - vacation';
  }

  /**
   * Used to get values from datepicker
   */
  getRange(range: { from: Date; to: Date }): void {
    console.log(range.from);
    console.log(range.to);

    this.periodStart = range.from;
    this.periodEnd = range.to;
  }

  createRequest(): void {
    if (this.periodStart && this.periodEnd && this.title) {
      this.loading = true;
      const comment = this.comment;
      this.requestService
        .createRequest(this.title, this.periodStart, this.periodEnd)
        .then(async (result) => {
          if (comment) {
            await this.requestService.addComment(result.id, comment);
          }

          this.loading = false;
          if (result) {
            this.create.emit(result);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
}
