import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {RequestService} from '../../../services/request/request.service';
import {Router} from '@angular/router';
import {Request} from '../../../interfaces/request';

@Component({
  selector: 'app-update-request',
  templateUrl: './update-request.component.html',
  styleUrls: ['./update-request.component.scss']
})
export class UpdateRequestComponent implements OnInit {

  loading = false;
  request: Request;

  @Input() requestInput: Request;
  @Output() update = new EventEmitter<Request>();

  constructor(private userService: UserService, private requestService: RequestService, private route: Router) {
  }

  ngOnInit(): void {
    this.request = Object.assign({}, this.requestInput);
  }

  /**
   * Used to get values from datepicker
   */
  getRange(range: { from: Date, to: Date }): void {
    this.request.periodStart = range.from;
    this.request.periodEnd = range.to;
  }

  updateRequest(): void {
    if (this.request.periodStart && this.request.periodEnd && this.request.title) {
      this.loading = true;
      this.requestService.updateRequest(this.request).then(
        async (result) => {

          this.loading = false;
          this.update.emit(result);
        }
      ).catch(
        (err) => {
          console.log(err);
        }
      );
    }
  }
}
