import { Component, OnInit, Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-loading-overlay',
  templateUrl: './loading-overlay.component.html',
  styleUrls: ['./loading-overlay.component.scss'],
  animations: [
    trigger('outAnimation', [
      transition(':leave', [animate('500ms', style({ opacity: 0 }))])
    ])
  ]
})
export class LoadingOverlayComponent implements OnInit {
  @Input() isLoading: boolean;
  constructor() {}

  ngOnInit(): void {}
}
