import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NgbCalendar, NgbDate, NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-range-display',
  templateUrl: './range-display.component.html',
  styles: [`
    .custom-day {
      text-align: center;
      padding: 0.185rem 0.25rem;
      display: inline-block;
      height: 2rem;
      width: 2rem;
      pointer-events: none;
    }

    .custom-day.focused {
      background-color: #e6e6e6;
    }

    .custom-day.range {
      background-color: rgb(2, 117, 216);
      color: white;
    }

    .custom-day.weekend {
      background-color: lightgray;
    }

    .custom-datepicker .ngb-dp-header {
      padding: 0;
    }

    .content-wrap {
      display: flex !important;
      flex-direction: row !important;
      flex-wrap: wrap;
      justify-content: center;
    }

    ngb-datepicker {
      border: none;
    }
  `]
})
export class RangeDisplayComponent implements OnInit, OnChanges {
  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate;
  toDate: NgbDate | null = null;

  displayMonths = 1;

  @Input() from: Date;
  @Input() to: Date;

  constructor(public i18n: NgbDatepickerI18n, private calendar: NgbCalendar) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.fromDate = new NgbDate(changes.from.currentValue.getFullYear(), changes.from.currentValue.getMonth() + 1, changes.from.currentValue.getDate());
    this.toDate = new NgbDate(changes.to.currentValue.getFullYear(), changes.to.currentValue.getMonth() + 1, changes.to.currentValue.getDate());

    this.displayMonths = this.toDate.month - this.fromDate.month + 1;
  }

  isWeekend(date: NgbDate): boolean {
    return this.calendar.getWeekday(date) >= 6;
  }

  onDateSelection(date: NgbDate): void {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate): boolean {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate): boolean {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate): boolean {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  disabled = () => true;

}
