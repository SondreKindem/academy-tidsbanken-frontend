import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { User } from '../../interfaces/user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Request } from '../../interfaces/request';
import { RequestService } from '../../services/request/request.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup
} from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  userForm: FormGroup;
  passwordForm: FormGroup;
  user: User;

  formDisabled = true;
  saving = false;

  requests: Request[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private modalService: NgbModal,
    private requestService: RequestService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  private async getUpdatedUser(id: string): Promise<User> {
    if (id) return await this.userService.getUser(id);
    return this.userService.user;
  }

  async ngOnInit(): Promise<any> {
    this.route.paramMap.subscribe(async (params) => {
      const id = params.get('id');
      this.user = await this.getUpdatedUser(id);
      this.requests = await this.requestService.getRequestsForUser(
        this.user.id
      );

      this.userForm = this.formBuilder.group({
        firstName: new FormControl(
          {
            value: this.user.firstName,
            disabled: this.formDisabled
          },
          [Validators.required, Validators.maxLength(50)]
        ),
        lastName: new FormControl(
          {
            value: this.user.lastName,
            disabled: this.formDisabled
          },
          [Validators.required, Validators.maxLength(50)]
        ),
        email: new FormControl(
          {
            value: this.user.email,
            disabled: this.formDisabled
          },
          [Validators.required, Validators.email, Validators.maxLength(320)]
        ),
        isAdmin: new FormControl({
          value: this.user.isAdmin,
          disabled: this.formDisabled
        })
      });
    });

    this.passwordForm = this.formBuilder.group(
      {
        password1: ['', [Validators.required, Validators.minLength(5)]],
        password2: ['', Validators.required]
      },
      { validator: this.passwordMatchValidator }
    );
  }

  passwordMatchValidator(frm: FormGroup) {
    return frm.controls['password1'].value === frm.controls['password2'].value
      ? null
      : { mismatch: true };
  }

  toggleEditing(): void {
    if (!this.userForm) return;

    this.formDisabled = !this.formDisabled;
    const { controls } = this.userForm;

    this.userForm.reset();
    for (const c in controls) {
      controls[c].setValue(this.user[c]);
      if (this.formDisabled) {
        controls[c].disable();
      } else {
        if (c !== 'isAdmin' || this.userIsAdmin) {
          controls[c].enable();
        }
      }
    }
  }

  onUserEditSubmit(): void {
    if (this.userForm.invalid) return;
    const { firstName, lastName, email } = this.userForm.value;

    const user: User = { ...this.user, ...this.userForm.value };
    console.log(this.userForm.value);

    console.log(user);

    this.saving = true;
    this.userService.updateUser(user).finally(() => {
      this.getUpdatedUser(user.id).then((u) => {
        console.log(u);

        this.user = u;
        this.saving = false;
        this.toggleEditing();
      });
    });
  }

  openPasswordModal(content): void {
    this.passwordForm.reset();
    this.modalService.open(content);
  }

  onPasswordChangeSubmit(modal): void {
    if (this.passwordForm.invalid) return;

    const { password1, password2 } = this.passwordForm.value;

    this.saving = true;
    this.userService
      .changePassword(this.user.id, password1, password2)
      .finally(() => {
        this.saving = false;
        modal.close();
      });
  }

  openImageModal(content): void {
    this.modalService.open(content).result.then(
      (result) => {
        console.log('SAVE?');
      },
      (rejected) => {
        console.log('CANCEL');
      }
    );
  }

  openConfirmDeleteModal(event, id: number, modal): void {
    event.stopPropagation();
    this.modalService.open(modal).result.then(
      (result) => {
        this.deleteRequest(id);
      },
      (rejected) => {
        console.log('CANCEL');
      }
    );
  }

  openCreateModal(modal): void {
    this.modalService.open(modal);
  }

  handleRequestCreated(modal, request: Request): void {
    this.modalService.dismissAll();
    this.router.navigate(['/request', request.id]);
  }

  requestClick(requestId: number): void {
    console.log('clicked event');
    this.router.navigate(['/request', requestId]);
  }

  async deleteRequest(id: number): Promise<any> {
    if (await this.requestService.deleteRequest(id)) {
      this.requests = this.requests.filter((r) => r.id !== id);
    }
  }

  canEdit(): boolean {
    return this.userIsLoggedIn() || this.userService.checkUser();
  }

  get userIsAdmin(): boolean {
    return this.userService.user.isAdmin;
  }
  /**
   * The displayed user is logged in
   */
  userIsLoggedIn(): boolean {
    return this.user.id === this.userService.user.id;
  }
}
