import {Component, OnInit} from '@angular/core';
import {User} from '../../interfaces/user';
import {UserService} from '../../services/user/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Request} from '../../interfaces/request';
import {RequestService} from '../../services/request/request.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {

  allUsers: User[];
  pendingRequests: Request[];

  newUser = {
    firstName: '',
    lastName: '',
    email: '',
    isAdmin: false,
  };

  constructor(private userService: UserService, private modalService: NgbModal, private requestService: RequestService, private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getAllUsers().then(
      (users) => {
        if (users) {
          this.allUsers = users;
          console.log(users);
        }
      }
    );
    this.requestService.getAllRequests().then(
      (result) => {
        // Only show pending requests that are not owned by the current user
        this.pendingRequests = result.filter(request => request.state === 'Pending' && request.ownerId !== this.userService.user.id);
      }
    );
  }

  openConfirmDeleteModal(event, id: string, modal): void {
    event.stopPropagation();
    this.modalService.open(modal).result.then((result) => {
      this.userService.deleteUser(id).then(
        (success) => {
          if (success) {
            this.allUsers = this.allUsers.filter((user) => user.id !== id);
          }
        }
      );
    }, (rejected) => {
      console.log('CANCEL');
    });
  }

  openCreateUserModal(modal): void {
    this.modalService.open(modal);
  }

  createUser(): void {
    this.userService.createUser(this.newUser.firstName, this.newUser.lastName, this.newUser.email, this.newUser.isAdmin).then(
      (user) => {
        if (user) {
          this.allUsers.unshift(user);
        }
        this.modalService.dismissAll();
      }
    );
  }

  isCurrentUser(id: string): boolean {
    return this.userService.user.id === id;
  }

  userClick(id: string): void {
    this.router.navigate(['/profile', id]);
  }

  requestClick(id: number): void {
    this.router.navigate(['/request', id]);
  }

  approveRequest(event, request: Request): void {
    event.stopPropagation();
    this.requestService.setRequestState(request.id, 'Approved').then(
      (result) => {
        if (result) {
          this.removeFromList(request.id);
        }
      }
    );
  }

  denyRequest(event, request: Request): void {
    event.stopPropagation();
    this.requestService.setRequestState(request.id, 'Denied').then(
      (result) => {
        if (result) {
          this.removeFromList(request.id);
        }
      }
    );
  }

  private removeFromList(id: number): void {
    this.pendingRequests = this.pendingRequests.filter((request) => request.id !== id);
  }
}
