import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../services/settings/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  maxVacationLength: number;

  loading = false;

  constructor(private settingsService: SettingsService) {
    this.maxVacationLength = settingsService.getMaxVacationLength();
  }

  save(): void {
    this.loading = true;

    this.settingsService.updateSettings(this.maxVacationLength).finally(
      () => {
        this.loading = false;
      }
    );
  }

  ngOnInit(): void {
  }

}
