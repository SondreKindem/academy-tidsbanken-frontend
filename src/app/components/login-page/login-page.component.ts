import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  OnInit
} from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { SettingsService } from '../../services/settings/settings.service';
import { RequestService } from '../../services/request/request.service';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup
} from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  emailForm: FormGroup;

  isLoggingIn = true;
  passwordReset = false;
  loading = false;

  constructor(
    private userService: UserService,
    private route: Router,
    private settingsService: SettingsService,
    private requestService: RequestService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  switchView(): boolean {
    this.isLoggingIn = !this.isLoggingIn;
    return false;
  }

  async loginSubmit() {
    if (this.loginForm.invalid) return;
    const { email, password } = this.loginForm.value;
    this.loading = true;
    await this.userService.logIn(email, password);
    await this.settingsService.init();
    await this.requestService.init();

    this.loading = false;

    if (this.userService.user) {
      await this.route.navigate(['/']);
    }
  }

  async onPasswordReset() {
    if (this.emailForm.invalid) return;
    const { email } = this.emailForm.value;

    this.loading = true;
    await this.userService.resetPassword(email);
    this.passwordReset = true;
    this.loading = false;
  }
}
