import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  checkCommentEditable(){
    //if now - comment.timestamp < a day, then canEdit = true;
  }

  getComment(){ //pass param requestId --initial comment should be optional
    this.http.get('https://tidsbanken-api.azurewebsites.net/getcomment').toPromise().then(function (result: any) {} );
  }
  setComment(){ //post
    let body : {
      commentText: "", //userInput
      commentOwner: "", //user_id
      commentTime: "", //timestamp of last edit
      commentEditable: true, //comments should be editable for 24 hrs
    };
    this.http.post('https://tidsbanken-api.azurewebsites.net/setcomment', body).toPromise().then().catch();

  }
  putComment(){ //put/patch

  }
  dropComment(){ //delete param commentId

  }
}
