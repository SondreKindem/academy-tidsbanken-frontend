import {Injectable} from '@angular/core';
import {Request} from '../../interfaces/request';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {ToastService} from '../toast/toast.service';
import {UserService} from '../user/user.service';
import {Comment} from '../../interfaces/comment';


@Injectable({
  providedIn: 'root'
})
export class RequestService {

  requestsChanged: Subject<Request[]> = new Subject<Request[]>();
  private _allRequests: Request[] = [];

  constructor(private http: HttpClient, private toastService: ToastService, private userService: UserService) {
  }

  init(): Promise<any> {
    if (this.userService.user) {
      return this.getAllRequests();
    }
  }

  getAllRequests(): Promise<Request[]> {
    return this.http.get('https://tidsbanken-api.azurewebsites.net/request').toPromise().then(
      (result: { data }) => {
        // Convert date strings to dates
        result.data.map(
          (request: Request) => RequestService.formatDates(request)
        );
        // Return only the data
        this._allRequests = result.data;
        this.requestsChanged.next(this._allRequests);
        return result.data as Request[];
      }
    ).catch(
      err => {
        console.log(err);
        this.toastService.showError('Error fetching vacation requests');
        return null;
      }
    );
  }

  getRequestsForUser(id: string): Promise<Request[]> {
    return this.http.get('https://tidsbanken-api.azurewebsites.net/user/' + id + "/requests").toPromise().then(
      (result: {data}) => {
        console.log(result);
        result.data.map(
          (request: Request) => RequestService.formatDates(request)
        );
        return result.data as Request[];
      }
    ).catch(
      (err) => {
        console.log(err);
        return null;
      }
    )
  }

  getRequest(id: number): Promise<Request> {
    return this.http.get(`https://tidsbanken-api.azurewebsites.net/request/${id}`).toPromise().then(
      (result: { data: Request }) => {
        console.log(result);
        return RequestService.formatDates(result.data);
      }
    ).catch(
      (err) => {
        console.log(err);
        this.toastService.showError('Unable to get request');
        return null;
      }
    );
  }

  getComments(requestId: number): Promise<Comment[]> {
    return this.http.get(`https://tidsbanken-api.azurewebsites.net/request/${requestId}/comment`).toPromise().then(
      (result: { data: Comment[] }) => {
        console.log(result.data);
        result.data.map(
          (comment: Comment) => {
            comment.updatedOn = comment.updatedOn ? new Date(comment.updatedOn) : null;
            comment.createdOn = new Date(comment.createdOn);
            return comment;
          }
        );
        return result.data;
      }
    ).catch(
      (err) => {
        console.log(err);
        this.toastService.showError('Unable to get comments');
        return null;
      }
    );
  }

  addComment(requestId: number, message: string): Promise<Comment> {
    return this.http.post(`https://tidsbanken-api.azurewebsites.net/request/${requestId}/comment`, {message}).toPromise().then(
      (result) => {
        console.log(result);
        this.toastService.showSuccess('Comment added');
        const comment: Comment = result['data'];
        comment.updatedOn = new Date(comment.updatedOn);
        comment.createdOn = new Date(comment.createdOn);
        return comment;
      }
    ).catch(
      (err) => {
        console.log(err);
        this.toastService.showError('Unable to create comment');
        return null;
      }
    );
  }

  updateComment(requestId: number, commentId: number, message: string): Promise<Comment> {
    return this.http.patch(`https://tidsbanken-api.azurewebsites.net/request/${requestId}/comment/${commentId}`, {message}).toPromise().then(
      result => {
        console.log(result);
        this.toastService.showSuccess('Updated comment');
        const comment: Comment = result['data'];
        comment.updatedOn = new Date(comment.updatedOn);
        comment.createdOn = new Date(comment.createdOn);
        return comment;
      }
    ).catch(
      err => {
        console.log(err);
        this.toastService.showError('Unable to update comment');
        return null;
      }
    );
  }

  createRequest(title: string, periodStart: Date, periodEnd: Date): Promise<Request> {
    return this.http.post('https://tidsbanken-api.azurewebsites.net/request', {title, periodStart, periodEnd}).toPromise().then(
      (result) => {
        console.log(result);
        this.toastService.showSuccess('Added request');
        this._allRequests.push(RequestService.formatDates(result['data']));
        return result['data'];
      }
    ).catch(
      (error) => {
        console.log(error);
        this.toastService.showErrors(error['error']['errors'], 'Error creating request');
      }
    );
  }

  setRequestState(id: number, state: string): Promise<boolean> {
    return this.http.patch('https://tidsbanken-api.azurewebsites.net/request/' + id, {state}).toPromise().then(
      () => true
    ).catch(
      (err) => {
        console.log(err);
        return false;
      }
    );
  }

  updateRequest(request: Request): Promise<Request> {
    const body = {
      title: request.title,
      periodStart: request.periodStart,
      periodEnd: request.periodEnd
    };
    return this.http.patch('https://tidsbanken-api.azurewebsites.net/request/' + request.id, body).toPromise().then(
      (result) => {
        this.toastService.showSuccess('Updated request');
        return RequestService.formatDates(result['data']);
      }
    ).catch(
      (err) => {
        console.log(err);
        this.toastService.showError('Unable to update request');
        return null;
      }
    );
  }

  deleteRequest(id: number): Promise<boolean> {
    return this.http.delete('https://tidsbanken-api.azurewebsites.net/request/' + id).toPromise().then(
      (result) => {
        // Remove request from list
        this._allRequests = this._allRequests.filter((request, index) => {
          return request.id !== id;
        });

        // Notify of change
        this.requestsChanged.next(this._allRequests);

        this.toastService.showSuccess('Deleted request');
        return true;
      }
    ).catch(
      (err) => {
        this.toastService.showError('Error deleting vacation request');

        console.log(err);
        return false;
      }
    );
  }

  private static formatDates(request: Request): Request {
    request.periodStart = new Date(request.periodStart);
    request.periodEnd = new Date(request.periodEnd);
    return request;
  }

  get allRequests(): Request[] {
    return this._allRequests;
  }
}
