import {Injectable, Injector} from '@angular/core';
import {UserService} from '../user/user.service';
import {HttpClient} from '@angular/common/http';
import {ToastService} from '../toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private maxVacationLength;

  constructor(private injector: Injector, private userService: UserService, private http: HttpClient, private toastService: ToastService) {
  }

  async init(): Promise<any> {
    // Wait for userService to init
    if (this.userService.fetchingPromise) {
      await this.userService.fetchingPromise;
    }

    if (!this.userService.user) {
      return;
    }

    return this.http.get('https://tidsbanken-api.azurewebsites.net/settings').toPromise().then(
      result => {
        this.maxVacationLength = result['data'].maxVacationLength;
        console.log(result);
      }
    ).catch(e => {
      console.log(e);
      this.toastService.show('Error getting settings', {classname: 'bg-danger text-light', delay: 10000});
    });
  }

  public getMaxVacationLength(): number {
    return this.maxVacationLength;
  }

  public updateSettings(maxVacationLength: number): Promise<any> {
    if (this.userService.checkUser) {
      return this.http.put('https://tidsbanken-api.azurewebsites.net/settings', {maxVacationLength}).toPromise().then(
        (response) => {
          this.maxVacationLength = maxVacationLength;
          this.toastService.show('Saved settings successfully', {classname: 'bg-success text-light', delay: 10000});
        }
      ).catch(
        (e) => {
          this.toastService.show(e['error']['errors'][0][0], {classname: 'bg-danger text-light', delay: 10000});
        }
      );
    } else {
      this.toastService.show('User must be admin to modify settings', {classname: 'bg-danger text-light', delay: 10000});
    }
  }
}
