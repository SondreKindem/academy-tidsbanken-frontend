import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserService} from './user/user.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.userService.user || this.userService.refreshTokenExpired()) {
      // If there is a promise, then we are probably trying to refresh the token
      if (this.userService.fetchingPromise) {

        this.userService.fetchingPromise.then(() => {
          // Refresh worked, user is authenticated
          return true;
        }).catch(() => {
          // Refresh failed, user must login again
          this.router.navigate(['login']);
        });

      } else{
        // If there is no promise, the user has not authenticated yet
        this.router.navigate(['login']);
      }
    }
    return true;
  }
}

