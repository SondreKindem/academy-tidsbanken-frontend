import {Injectable} from '@angular/core';
import {HttpInterceptor} from '@angular/common/http';
import {HttpRequest} from '@angular/common/http';
import {HttpHandler} from '@angular/common/http';
import {HttpEvent} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {UserService} from './user/user.service';
import {EMPTY, from, Observable} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ToastService} from './toast/toast.service';

/**
 * Add access token to request headers
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private userService: UserService, private router: Router, private toastService: ToastService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // If the user has not logged in yet, there is no accesstoken
    if (!this.userService.user || request.headers.get('skip')) {
      return next.handle(request);
    }

    // If we try to get a request, and token is expired, redirect to login
    if (this.userService.refreshTokenExpired() && this.router.url !== '/login') {
      this.router.navigate(['/login']);
      this.toastService.showError('Login expired');
    }

    // If userService is loading, wait for load
    if (this.userService.fetchingPromise) {
      return from(this.userService.fetchingPromise).pipe(
        switchMap(
          () => {
            return this.modifyReq(request, next);
          }
        )
      );
    }

    // Refresh token if expired, then complete request
    return from(this.userService.getNewTokenIfExpired()).pipe(
      take(1),
      switchMap(() => {
        return this.modifyReq(request, next);
      })
    );
  }

  private modifyReq(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.userService.accessToken}`
    });

    // Pass through a clone of the original request, but with auth header
    return next.handle(request.clone({headers}));
  }
}
