import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { HttpClient } from '@angular/common/http';
import jwt_decode from 'jwt-decode';
import { ToastService } from '../toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _user: User = null;
  private _accessToken: string = null;
  private _refreshToken: string = null;

  // A promise that indicates re
  private _fetchingPromise: Promise<any> = null;

  constructor(private http: HttpClient, private toastService: ToastService) {}

  get user(): User {
    return this._user;
  }

  get accessToken(): string {
    return this._accessToken;
  }

  get fetchingPromise(): Promise<any> {
    return this._fetchingPromise;
  }

  private static checkTokenExpired(token: string): boolean {
    const tokenData: TokenData = UserService.getDecodedAccessToken(token);
    return Math.abs(Date.now() / 1000) >= tokenData.exp;
  }

  private static saveTokensLocal(
    accessToken: string,
    refreshToken: string
  ): void {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('refreshToken', refreshToken);
  }

  private static saveTokensSession(
    accessToken: string,
    refreshToken: string
  ): void {
    sessionStorage.setItem('accessToken', accessToken);
    sessionStorage.setItem('refreshToken', refreshToken);
  }

  private static getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  async init(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      // Check if tokens exist and are not expired
      if (
        localStorage.getItem('accessToken') &&
        localStorage.getItem('refreshToken')
      ) {
        this._refreshToken = localStorage.getItem('refreshToken');
        const accessToken = localStorage.getItem('accessToken');
        if (UserService.checkTokenExpired(accessToken)) {
          if (!this.refreshTokenExpired()) {
            console.log('Trying to refresh token...');
            await this.refreshToken();
          }
        } else {
          console.log('parsing token');
          this.parseToken(accessToken);
        }
      }

      resolve();
    });
  }

  /**
   * Check if user is admin
   */
  checkUser(): boolean {
    if (this._user) {
      return this._user.isAdmin;
    }
    return false;
  }

  refreshToken(): Promise<any> {
    const body = {
      granttype: 'refresh_token',
      refreshtoken: this._refreshToken
    };

    // Set the promise field to the promise from the refresh request.
    // The loginGuard uses this to wait for refresh before redirecting
    return this.handleApiLogin(body);
  }

  async getNewTokenIfExpired(): Promise<any> {
    return new Promise<any>(async (resolve) => {
      if (UserService.checkTokenExpired(this._accessToken)) {
        await this.refreshToken();
      }
      resolve();
    });
  }

  refreshTokenExpired(): boolean {
    if (!this._refreshToken) {
      return true;
    }
    return UserService.checkTokenExpired(this._refreshToken);
  }

  async logIn(username: string, password: string): Promise<any> {
    const body = {
      username,
      password,
      granttype: 'password'
    };

    await this.handleApiLogin(body);
  }

  /**
   * Helper for sending request to login endpoint
   * @param body an object containing either user+pass or refresh token
   */
  private handleApiLogin(body: object): Promise<any> {
    this._fetchingPromise = this.http
      .post('https://tidsbanken-api.azurewebsites.net/login', body, {
        headers: { skip: 'true' }
      })
      .toPromise()
      .then((result: LoginResult) => {
        if (result.errors) {
          throw new Error('unknown error');
        }

        this.parseToken(result.data.accessToken);

        UserService.saveTokensLocal(
          result.data.accessToken,
          result.data.refreshToken
        );
        this._refreshToken = result.data.refreshToken;

        // this.route.navigate(['/']);
      })
      .catch((e) => {
        console.log(e);
        this.toastService.showError('Error logging in');
        // this.toastService.show(e.error.errors[Object.keys(e.error.errors)[0]][0], {classname: 'bg-danger text-light', delay: 10000});
      });

    return this._fetchingPromise;
  }

  logOut(): void {
    this._user = null;
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('refreshToken');
  }

  /**
   * get user info from access token
   * @param accessToken an encoded jwt string
   */
  private parseToken(accessToken: string): void {
    const tokenData: TokenData = UserService.getDecodedAccessToken(accessToken);
    console.log(UserService.getDecodedAccessToken(accessToken));
    this._user = {
      id: tokenData.sub,
      email: tokenData.email,
      name: tokenData.name,
      firstName: tokenData.given_name,
      lastName: tokenData.family_name,
      image:
        tokenData.image ??
        'https://api.adorable.io/avatars/150/' + tokenData.email,
      isAdmin: tokenData.roles ? tokenData.roles.includes('admin') : false
    };
    this._accessToken = accessToken;
  }

  createUser(firstName, lastName, email, isAdmin: boolean): Promise<User> {
    const body = { firstName, lastName, email, isAdmin };
    return this.http
      .post('https://tidsbanken-api.azurewebsites.net/user', body)
      .toPromise()
      .then((result) => {
        this.toastService.showSuccess('Created user');
        return result['data'];
      })
      .catch((err) => {
        console.log(err);
        this.toastService.showError('Could not create user');
      });
  }

  getUser(id: string): Promise<User> {
    return this.http
      .get('https://tidsbanken-api.azurewebsites.net/user/' + id)
      .toPromise()
      .then((result) => {
        console.log(result);
        const user: User = result['data'];
        user.image =
          user.image ?? 'https://api.adorable.io/avatars/150/' + user.email;
        // Cheat to get first and second name >:)
        user.firstName = user.name.substr(0, user.name.indexOf(' '));
        user.lastName = user.name.substr(user.name.indexOf(' ') + 1);
        return user;
      })
      .catch((err) => {
        console.log(err);
        return null;
      });
  }

  deleteUser(id: string): Promise<boolean> {
    return this.http
      .delete('https://tidsbanken-api.azurewebsites.net/user/' + id)
      .toPromise()
      .then(() => {
        this.toastService.showSuccess('Deleted user');
        return true;
      })
      .catch((err) => {
        console.log(err);
        this.toastService.showError('Could not delete user');
        return false;
      });
  }
  updateUser(user): Promise<any> {
    return this.http
      .patch('https://tidsbanken-api.azurewebsites.net/user/' + user.id, user)
      .toPromise()
      .then((result: any) => {
        console.log(result);
        if (result.data.id === this._user.id) {
          return this.refreshToken();
        }
      });
  }

  changePassword(userId, password1, password2): Promise<any> {
    return this.http
      .post(
        'https://tidsbanken-api.azurewebsites.net/user/' +
          userId +
          '/reset-password',
        { password1, password2 }
      )
      .toPromise()
      .then((result) => console.log(result));
  }

  resetPassword(email): Promise<any> {
    return this.http
      .post('https://tidsbanken-api.azurewebsites.net/user/forgot-password', {
        email
      })
      .toPromise();
  }

  getAllUsers(): Promise<User[]> {
    return this.http
      .get('https://tidsbanken-api.azurewebsites.net/user/all')
      .toPromise()
      .then((result) => {
        console.log(result);
        return result['data'];
      });
  }
}

interface LoginResult {
  errors: any;
  data: { accessToken: string; refreshToken: string };
}

interface TokenData {
  sub: string;
  email: string;
  name: string;
  image: string;
  exp: number;
  roles: string[];
  given_name: string;
  family_name: string;
}
