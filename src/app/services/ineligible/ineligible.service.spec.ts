import { TestBed } from '@angular/core/testing';

import { IneligibleService } from './ineligible.service';

describe('IneligibleService', () => {
  let service: IneligibleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IneligibleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
