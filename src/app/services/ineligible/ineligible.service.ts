import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class IneligibleService {

  private listOfIneligible;

  constructor(private userService: UserService, private http: HttpClient) { }

  async fetchAll() : Promise<any>{
    return this.http.get("https://tidsbanken-api.azurewebsites.net/ineligible").toPromise().then((result: any) => {
      return result;
    });
  }

  async createIneligible(period): Promise<any>{
    if (this.userService.checkUser()) {
      var body = {
      start: period.from,
      end: period.to
    };
    return this.http.post("https://tidsbanken-api.azurewebsites.net/ineligible", body).toPromise();
    }else{
      alert("Something went wrong, are you admin?");
    }
  }

  async updateIneligible(id, period): Promise<any>{
      if (this.userService.checkUser()) {
        var body = {
        start: period.from,
        end: period.to
      };
      return this.http.patch(`https://tidsbanken-api.azurewebsites.net/ineligible/${id}`, body).toPromise();
    } else {
      alert("something went wrong, are you admin?");
    }
  }

  async deleteIneligible(id){
      if (this.userService.checkUser()) {
      this.http.delete(`https://tidsbanken-api.azurewebsites.net/ineligible/${id}`).toPromise();
    } else {
      alert("something went wrong, are you admin?");
    }
  }
}


