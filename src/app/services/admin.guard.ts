import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {UserService} from './user/user.service';
import {ToastService} from './toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private userService: UserService, private toastService: ToastService) {
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    if (this.userService.fetchingPromise) {
      await this.userService.fetchingPromise;
    }

    // Only admins allowed! >:(
    if (this.userService.user && this.userService.checkUser()) {
      return true;
    } else {
      this.toastService.show('You must be admin to access this', {classname: 'bg-danger text-light', delay: 5000});
      return false;
    }
  }
}
