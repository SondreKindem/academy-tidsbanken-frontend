import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, InjectionToken, NgModule } from '@angular/core';

import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
// import interactionPlugin from '@fullcalendar/interaction'; // a plugin

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DatepickerComponent } from './components/shared/datepicker/datepicker.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastsContainerComponent } from './components/toasts-container/toasts-container.component';
import { CommentComponent } from './components/comment/comment.component';
import { RequestComponent } from './components/request/request.component';
import { SettingsService } from './services/settings/settings.service';
import { AuthInterceptor } from './services/auth.interceptor';
import { UserService } from './services/user/user.service';
import { SettingsComponent } from './components/settings/settings.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestService } from './services/request/request.service';
import { IconsModule } from './icons/icons.module';
import { RangeDisplayComponent } from './components/shared/range-display/range-display.component';
import { CreateRequestComponent } from './components/shared/create-request/create-request.component';
import { SortByPipe } from './pipes/sort-by.pipe';
import { UpdateRequestComponent } from './components/shared/update-request/update-request.component';
import { ManagementComponent } from './components/management/management.component';
import { LoadingOverlayComponent } from './components/shared/loading-overlay/loading-overlay.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin
  // interactionPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DatepickerComponent,
    NavbarComponent,
    DashboardComponent,
    NavbarComponent,
    UserProfileComponent,
    LoginPageComponent,
    ToastsContainerComponent,
    CommentComponent,
    RequestComponent,
    SettingsComponent,
    RangeDisplayComponent,
    CreateRequestComponent,
    SortByPipe,
    UpdateRequestComponent,
    ManagementComponent,
    LoadingOverlayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FullCalendarModule,
    FormsModule,
    ReactiveFormsModule,
    IconsModule,
    BrowserAnimationsModule
    // register FullCalendar with you app
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: configurationFactory,
      deps: [UserService, RequestService],
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: settingsFactory,
      deps: [SettingsService],
      multi: true
    },
    AuthInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function settingsFactory(settingsService: SettingsService): any {
  return () => settingsService.init();
}

export function configurationFactory(
  userService: UserService,
  requestService: RequestService
): () => Promise<any> {
  return async (): Promise<any> => {
    await userService.init();
    // init all deps
    if (userService.user) {
      // Requests must be init after userService!
      requestService.init();
    }
  };
}
